(function(window){

var sample_markers = [];

var ProsodicMap = function() {

  // Read parameters from URL (at this point only center and zoom)
  var params = {
    center: [-36.95574, -70.04883],
    zoom: 4,
  };
  window.location.href.replace(
    /[?&]+([^=&]+)=([^&]*)/gi,
    function(m, key, value) {
      params[key] = value;
    }
  );

  // Initialize map with URL parameters or default values
  this.map = L.map('map', {
    center: params.center,
    zoom: params.zoom,
    minZoom: 4,
  }).on('click', onMapClick);
  this.zone_layers = [];

  // Layer control. Currently disabled.
  // this.markers = new L.LayerGroup([]);
  // this.overlays = { "Markers": this.markers };
  // L.control.layers({}, this.overlays).addTo(this.map);

  // Scale control
  L.control.scale().addTo(this.map);

  this.area   = $('#area-selector')[0].value;
  this.task   = $('#task-selector')[0].value;
  this.gender = $('#gender-selector')[0].value;

  this.load_zone = function(zone) {
    this.zone = zone;
    this.speakers = [];

    var index = zone.properties.index;
    var layer = this.zone_layers[ index - 1];
    this.map.fitBounds(layer.getBounds());

    $('#zone-nav ul li button').removeClass('active-button');
    $('#zone-nav ul')
      .children().eq(index - 1)
      .children('button').addClass('active-button');

    $('#intro').addClass('hidden');
    $('#zone').removeClass('hidden');

    $('#zone > h2').text('Zona ' + index);

    var self = this;
    if (!sample_markers[ index - 1 ]) {
//       console.log('Initialising zone ', index);
      $.getJSON('geojson/zone' + index + '.json', function(data) {
        $.each(data.features, function (i, point) {
          prepareSound(point.properties);
          self.speakers.push(point);
        });
        self.update();
      });
    }
  }

  this.set_task = function( val ) {
    this.task = val;
    this.update();
  }

  this.set_area = function( val ) {
    this.area = val;
    this.update();
  }

  this.set_gender = function( val ) {
    this.gender = val;
    this.update();
  }

  this.update = function() {
    var self = this;
    var samples = this.speakers.filter(function(o) {
      var p = o.properties;
      return p.gender == self.gender
          && p.area   == self.area
          && p.task   == self.task;
    });

    if (this.marker) {
      this.map.removeLayer(this.marker);
    }

//     console.log('Currently on zone ' + this.zone.properties.index);
//     console.log('Area: ' + this.area);
//     console.log('Task: ' + this.task);
//     console.log('Gender: ' + this.gender);
//     console.log('Found ' + samples.length + ' samples');

    if (samples.length == 0) {
      $('#sample').addClass('hidden');
      $('#no-sample').removeClass('hidden');
      return;
    }
    else {
      $('#no-sample').addClass('hidden');
      $('#sample').removeClass('hidden');
      if (samples.length == 1) {
        $('#sample-list').addClass('hidden');
      }
      else {
        $('#sample-list').removeClass('hidden');
        $('#sample-list > ul').html('');

        var self = this;
        $.each(samples, function (i, sample) {
          var button = $('<button>' + ( i + 1) + '</button>')
            .click(function(){
              self.load_sample(samples[i]);
            });
          var item = $('<li></li>').append(button);
          $('#sample-list > ul').append(item);
        });
      }
    }

    this.load_sample(samples[0]);
  }

  this.load_sample = function(sample) {
    var paths = sample.properties.path;

    var coord = sample.geometry.coordinates;
    this.marker = L.marker([ coord[1], coord[0] ], {
      icon: L.icon({
        iconUrl: 'images/' +
          sample.properties.area + '-' + sample.properties.gender + '.png',
          iconAnchor: [25, 25],
      }),
    }).addTo(this.map);

    $('#sound-figure > img').attr('src', 'images/' + paths.image);
    $('#sound-link').attr('href', 'audio/' + paths.audio);
    $('#hum-link').attr('href', 'audio/' + paths.hum);
  }

  this.from_number = function(n, method) {
    var self = this;
    jQuery.ajax({
      type: "GET",
      cache: false,
      url: "geojson/zones.json",
      dataType: "json",
      success: function(data) {
          self[method]( data.features[ n - 1 ] );
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        alert("Error processing GeoJSON data: " + errorThrown);
      },
    });
  }

  this.colombia = function() {
    var self = this;
    this.zone = {
      properties: {
        index: '?',
        name: 'Colombia'
      }
    };

    this.speakers = [];

    $('#zone-nav ul li button').removeClass('active-button');
    $('#zone-nav ul')
      .children().eq(8)
      .children('button').addClass('active-button');

    $('#intro').addClass('hidden');
    $('#zone').removeClass('hidden');

    $('#zone > h2').text('Medellín, Colombia');

    jQuery.ajax({
      type: "GET",
      cache: false,
      url: "geojson/colombia.json",
      dataType: "json",
      success: function(data) {
        $.each(data.features, function (i, point) {
          prepareSound(point.properties);
          self.speakers.push(point);
        });

        var coord = data.features[0].geometry.coordinates;
        self.map.panTo(new L.LatLng(coord[1], coord[0]));

        self.update();
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        alert("Error processing GeoJSON data: " + errorThrown);
      },
    });
  }

  // Read tiles
  // Currently using OpenStreetMap, but maybe a different
  // tile provider would be better
  L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    attribution: local_strings["map-attribution"],
    maxZoom: 18
  }).addTo(this.map);

  function prepareSound(sample) {
    var sid = sample.path.audio.replace(/\.\w+$/, '');
    var snd = soundManager.getSoundById(sid);
    if (!snd) {
      // Create new sound
      soundManager.createSound({
        id: sid,
        url: 'audio/' + sample.path.audio
      });
    }
  }

  // Temporary function to style features
  // As more features are added, maybe type-specific
  // functions are more efficient
  this.style_feature = function(feature) {
    var styles = [
      { color: "#dc322f", opacity: 1, weight: 1 },
      { color: "#cb4b16", opacity: 1, weight: 1 },
      { color: "#b58900", opacity: 1, weight: 1 },
      { color: "#859900", opacity: 1, weight: 1 },
      { color: "#b58900", opacity: 1, weight: 1 },
      { color: "#859900", opacity: 1, weight: 1 },
      { color: "#b58900", opacity: 1, weight: 1 },
      { color: "#6c71c4", opacity: 1, weight: 1 },
      { color: "#268bd2", opacity: 1, weight: 1 },
    ];

    return styles[feature.properties.index];
  }

  function loadSound(sample) {
    var sid = sample.path.audio.replace(/\.\w+$/, '');
    soundManager.stopAll();
    soundManager.play(sid);
    // $('.leaflet-popup-content div figure figcaption').text(sample.gender + ' ' + sample.zone);
    $('.leaflet-popup-content div figure img').attr('src', 'images/' + sample.path.image);
  }

  // Map onclick function
  // So far mostly a placeholder function useful for debug
  function onMapClick(e) {
    console.log("You clicked the map at " + e.latlng);
  }

};

window.ProsodicMap = ProsodicMap;

})(window)
