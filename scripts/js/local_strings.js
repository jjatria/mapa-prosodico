var local_strings = {
  "button-label-female-urban": "Mujer urbana",
  "button-label-female-rural": "Mujer rural",
  "button-label-male-urban": "Hombre urbano",
  "button-label-male-rural": "Hombre rural",
  "map-attribution": "Mapa e imágenes de <a href='http://openstreetmap.org'>OpenStreetMap</a> (<a href='http://creativecommons.org/licenses/by-sa/2.0/'>CC-BY-SA</a>)",
  "main-title": "Mapa Prosódico"
};
