﻿################################
### MAPA PROSÓDICO DE CHILE
################################
## 2014, junio
## Script inicial
## Objetivos: 
##   Dar un nombre estándar al archivo
## 
## El script presenta un formulario para escribir los datos del audio que se
## analiza y genera un nombre estándar para el sonido. Tras maximizar la
## intensidad, el script genera un TextGrid y termina.

## Formulario
form Nombrar sonido...

  optionmenu Informante_(*) 1
    option 1
    option 2
    option 3
    option 4
    option 5

  optionmenu Sexo_(*) 1
    option ...
    option Hombre
    option Mujer

  optionmenu Prueba_(*) 1
    option ...
    option Conversación inicial     
    option Lectura de frases        
    option Lectura de textos        
    option Enumeraciones de imágenes
    option DCT                      
    option Retextualización         
    option Conversación final       

  optionmenu Zona_(*) 1
    option ...
    option 1: Arica-Iquique
    option 2: Antofagasta
    option 3: La Serena-Coquimbo
    option 4: Santiago-Valparaíso
    option 5: Chillán-Concepción-Los Ángeles
    option 6: Temuco-Valdivia
    option 7: Castro-Ancud
    option 8: Punta Arenas

  optionmenu Tipo_(*) 1
    option ...
    option Rural
    option Urbana

  sentence Localidad 

  positive Identificador 1

  comment Los campos marcados con un asterisco son obligatorios
  
endform

if sexo == 1 or prueba == 1 or zona == 1 or tipo == 1
  exitScript: "Campo obligatorio incompleto"
endif

if !selected("Sound") and !numberOfSelected() == 1
  exitScript: "Por favor selecciona un objeto Sound"
endif

### Audio seleccionado
audio = selected("Sound")

### Normaliza la amplitud
Scale peak: 0.99

zona = zona - 1

# Variables iniciales
largo_zona     = 1
largo_sexo     = 1
largo_local    = 4
separador$     = "-"
sin_mayusculas = 1

# Codigo de zona: 1{r,u}
codigo_zona$ = string$(zona) + left$(tipo$, largo_zona)

# Codigo de participante: {m,f}1
codigo_informante$ = left$(sexo$, largo_sexo) + string$(informante)

# Codigo de tipo de prueba: aaa 
codigo_prueba$ = if prueba == 2 then "cvi" else
  ...            if prueba == 3 then "frs" else
  ...            if prueba == 4 then "txt" else
  ...            if prueba == 5 then "enm" else
  ...            if prueba == 6 then "dct" else
  ...            if prueba == 7 then "rtx" else
  ...            if prueba == 8 then "cvf" else
  ...            "xxx" fi fi fi fi fi fi fi

# Encadenacion del nombre de archivo: 1a-a1-aaa
archivo$ =  codigo_zona$       + separador$ +
  ...       codigo_informante$ + separador$ +
  ...       codigo_prueba$
  
## Campos extra
# Identificador por item
if identificador
  archivo$ = archivo$ + separador$ + string$(identificador)
endif

# Detalle de localidad
if localidad$ != ""
  codigo_area$ = left$(localidad$, largo_local)
  archivo$ = archivo$ + separador$ + codigo_area$
endif

if sin_mayusculas
  archivo$ = replace_regex$(archivo$, "([A-Z])", "\L\1", 0)
endif

Rename: archivo$
textgrid = To TextGrid: "voc frs sil", "voc"

selectObject: audio, textgrid
